<aside id="sidebar">
    <div id="buscador" class="bloque">
        <h3>Buscar</h3>

        <form action="buscar.php" method="POST">
            <input type="text" name="busqueda">
            <input type="submit" name="submit" value="Buscar">
        </form>
    </div>
    <!--valimos si el login es correcto y le damos la bienvenida al usuario-->
    <?php if(isset($_SESSION['usuario'])):?>
    <div id="usuario-logueado" class="bloque">
        <h3>Bienvenido,
            <?= $_SESSION['usuario']['nombre'] . ' ' . $_SESSION['usuario']['apellidos'];?>
        </h3>
        <!--CREAMOS BOTONES-->
        <a href="crearCategoria.php" class="boton boton-verde">Crear Categoria</a>
        <a href="crearEntradas.php" class="boton">Crear Entradas</a>
        <a href="misdatos.php" class="boton boton-naranja">Mis Datos</a>
        <a href="cerrar.php" class="boton boton-rojo">Cerrar Sesion</a>

    </div>
    <?php endif; ?>
    <!-- fin de la bienvenida-->
    <?php if(!isset($_SESSION['usuario'])):?>

    <div id="login" class="bloque">
        <h3>Identificacion</h3>

        <!---validamos si hay un error-->
        <?php if(isset($_SESSION['error_login'])):?>
        <div class="alerta alerta-error">
            <?= $_SESSION['error_login'];?>
        </div>
        <?php endif; ?>
        <!-- fin de la validacion de error de login-->

        <form action="login.php" method="POST">
            <label for="email">Email</label>
            <input type="email" name="email">

            <label for="password">Contraseña</label>
            <input type="password" name="password">

            <input type="submit" name="submit" value="Entrar">
        </form>
    </div>

    <div id="register" class="bloque">

        <h3>Registro</h3>

        <!--MOSTRAMOS RESULTADOS DE GUARDAR USUARIO-->
        <?php if(isset($_SESSION['completado'])): ?>
        <div class='alerta alerta-exito'>
            <?=$_SESSION['completado']?>
        </div>
        <?php elseif(isset($_SESSION['errores']['general'])): ?>
        <div class='alerta alerta-error'>
            <?=$_SESSION['errores']['general']?>
        </div>
        <?php endif;?>
        <!--FIN DE MOSTRAR ERRORES DE GUARDAR USUARIO-->
        <form action="registro.php" method="POST">

            <label for="nombre">Nombre</label>
            <input type="text" name="nombre">
            <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'nombre') : ''; ?>

            <label for="apellido">Apellido</label>
            <input type="text" name="apellido">
            <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'apellido') : ''; ?>


            <label for="email">Email</label>
            <input type="email" name="email">
            <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'email') : ''; ?>


            <label for="password">Contraseña</label>
            <input type="password" name="password">
            <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'password') : ''; ?>


            <input type="submit" name="submit" value="Registrar">
        </form>
        <?php borrarErrores();?>

    </div>
    <?php endif; ?>

</aside>