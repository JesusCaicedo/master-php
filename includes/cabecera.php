<?php require_once 'conexion.php'; ?>
<?php require_once 'includes/helpers.php'; ?>

<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8" />
        <title>Blog Video Juegos</title>

        <link rel="stylesheet" type="text/css" href="./assets/css/style.css">
    </head>

    <body>
        <!--CABECERA-->
        <header id="cabecera">
            <!--LOGO-->
            <div id="logo">
                <a href="index.php">
                    Blog de Video Juegos
            </div>
            <!--MENU-->
            <nav id="menu">
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                    </li>

                    <!--obtengo las categorias de helpers-->
                    <?php $categorias = conseguirCategorias($db);
                        
                        if(!empty($categorias)):
                                //<!--recorro las categorias e imprimo en pantalla en lista-->
                                while($categoria = mysqli_fetch_assoc($categorias)): 
                            ?>
                    <li>
                        <a href="categoria.php?id=<?=$categoria['id']?>"><?=$categoria['nombre']?></a>
                    </li>

                    <?php 
                            endwhile;
                            endif;
                                

                    ?>
                    <!--fin de las categorias-->
                    <li>
                        <a href="sobremi.php">Sobre Mi</a>
                    </li>
                    <li>
                        <a href="contacto.php">Contacto</a>
                    </li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </header>
        <div id="contenedor">