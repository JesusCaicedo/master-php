<?php

    if(!isset($_POST['busqueda'])){
            header("location: index.php");
        }

    ?>

<?php require_once './includes/cabecera.php' ?>
<?php require_once './includes/lateral.php'?>
<div id="principal">


    <h1>Busqueda: <?= $_POST['busqueda']?></h1>

    <!--obtengo las entradas de helpers-->
    <?php $entradas = conseguirEntradas($db,null,null,$_POST['busqueda']);



            if(!empty($entradas) && mysqli_num_rows($entradas) >= 1):
            //<!--recorro las entradas e imprimo en pantalla en lista-->
            while($entrada = mysqli_fetch_assoc($entradas)): 
            //var_dump($entrada);

     ?>

    <article class="entrada">
        <a href="entrada.php?id=<?=$entrada['id']?>">
            <h2><?=$entrada['titulo']?></h2>

            <span class='fecha'><?= $entrada['categoria']. ' | '. $entrada['fecha'] ?></span>
            <p>
                <?=substr($entrada['descripcion'], 0, 180). "..."; ?>
            </p>
        </a>
    </article>

    <?php 
    endwhile;
    else:
    ?>

    <div class="alerta"> No hay resultados para la busqueda</div>
    <?php endif; ?>
    <!--fin de las entradas-->



</div>


<?php include_once './includes/footer.php'?>