<?php

//var_dump($_POST);

if(isset($_POST['submit'])){

require_once './includes/conexion.php';

//validamos si la sesion no existe, de lo contrario la creamos
if(!isset($_SESSION)){
session_start();
}


//recogemos los datos y asignamos false si estan vacios, con mysql_escape evitamos las inyecciones SQL
    $nombre = isset($_POST['nombre']) ? mysqli_escape_string($db, $_POST['nombre']) : false;
    $apellido = isset($_POST['apellido']) ? mysqli_escape_string($db, $_POST['apellido']) : false;
    $email = isset($_POST['email']) ? mysqli_escape_string($db, trim($_POST['email'])) : false;
    $password = isset($_POST['password']) ? mysqli_escape_string($db, trim($_POST['password'])) : false;


    //array de errores
    $errores = array();

//Validamos si los datos no estan vacios y ademas si no es numerico y validamos que no contenga numeros
//validamos el nombre    
    if(!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/",$nombre)){
        $nombre_validado=true;
    }else{
        $nombre_validado=false;
        $errores['nombre'] = "El nombre no es valido";
    }

    //validamos el apellido
    if(!empty($apellido) && !is_numeric($apellido) && !preg_match("/[0-9]/",$apellido)){
            $apellido_validado=true;
        }else{
            $apellido_validado=false;
            $errores['apellido'] = "El apellido no es valido";
        }
        //validamos el email
        if(!empty($email) && filter_var($email,FILTER_VALIDATE_EMAIL)){
            $email_validado=true;
        }else{
            $email_validado=false;
            $errores['email'] = "El email no es valido";
        }

        //Validamos la contraseña
        if(!empty($password)){
            $password_validado=true;
        }else{
            $´password_validado=false;
            $errores['password'] = "La contraseña no es valida";
        }

        //seteamos una variable en false para luego cambiar los estados y saber que realiza, guardar o no el usuario
        $guardar_usuario = false;
        if(count($errores) == 0){
            $guardar_usuario = true;


            //CIFRAMOS LA INFORMACION DE ENTRADA

            $password_segura = password_hash($password,PASSWORD_BCRYPT,['cost'=>4]);
           
            //guardamos el usuario en su tabla correspondiente
           
            $sql = "INSERT INTO usuarios VALUES (null, '$nombre','$apellido','$email','$password_segura',CURDATE())";
            $guardar = mysqli_query($db,$sql);
            
            //validamos errores del sql
            //var_dump(mysqli_error($db));
            //die();
            
            if($guardar){
                $_SESSION['completado'] = "El usuario se registro exitosamente";
            }else{
                $_SESSION['errores']['general'] = "Fallo al guardar el usuario";
            }

        }else{

            /*si hay errores, los guardamos en una session indicamos 
            reidereccionar al index, para requeriel la funcion de mostrarError del archivo helper*/
            $_SESSION['errores'] = $errores;
        }
}

 header('location: index.php');

 ?>