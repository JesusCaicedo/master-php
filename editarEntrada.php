  <?php require_once './includes/redireccion.php'?>
  <?php require_once './includes/helpers.php' ?>
  <?php require_once './includes/conexion.php' ?>

  <?php
        $entrada_actual = conseguirEntrada($db,$_GET['id']);
        if(!isset($entrada_actual['id'])){
            header("location: index.php");
        }
    ?>


  <?php require_once './includes/cabecera.php' ?>
  <!--BARRA LATERAL-->
  <?php require_once './includes/lateral.php'?>


  <div id="principal">

      <!--MOSTRAMOS RESULTADOS DE GUARDAR USUARIO-->
      <?php if(isset($_SESSION['completado'])): ?>
      <div class='alerta alerta-exito'>
          <?=$_SESSION['completado']?>
      </div>
      <?php elseif(isset($_SESSION['errores']['entrada'])): ?>
      <div class='alerta alerta-error'>
          <?=$_SESSION['errores']['entrada']?>
      </div>
      <?php endif;?>
      <!--FIN DE MOSTRAR ERRORES DE GUARDAR USUARIO-->

      <h1>Editar Entradas</h1>

      <p>
          Edita la entrada <?= $entrada_actual['titulo'];?>
      </p>

      <form action="guardar-entradas.php?editar=<?=$entrada_actual['id']?>" method="POST">
          <br>


          <label for="titulo"> Titulo</label>
          <input type="text" name="titulo" id="titulo" value="<?=$entrada_actual['titulo'];?>">
          <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'titulo') : ''; ?>


          <label for="descripcion"> Descripcion</label>
          <textarea name="descripcion" id="descripcion" cols="80"
              rows="5"><?=$entrada_actual['descripcion'];?></textarea>
          <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'descripcion') : ''; ?>


          <label for="categoria"> Categoria</label>
          <select name="categoria" id="categoria">

              <?php $categorias = conseguirCategorias($db);
                        
              if(!empty($categorias)):
              //<!--recorro las categorias e imprimo en pantalla en lista-->
                while($categoria = mysqli_fetch_assoc($categorias)): 
            ?>
              <option value="<?=$categoria['id']?>"
                  <?=($categoria['id'] == $entrada_actual['categoria_id'] ? 'selected=selected' : '')?>>
                  <?=$categoria['nombre']?></option>
              <?php 
                 endwhile;
               endif;        
                    ?>

          </select>
          <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'categoria') : ''; ?>

          <input type="submit" value="Actualizar">

      </form>
      <?php borrarErrores();?>

  </div>





  <?php include_once './includes/footer.php'?>