<?php require_once './includes/redireccion.php'?>
<?php require_once './includes/cabecera.php' ?>
<?php require_once './includes/lateral.php'?>


<div id="principal">

    <!--MOSTRAMOS RESULTADOS DE GUARDAR USUARIO-->
    <?php if(isset($_SESSION['completado'])): ?>
    <div class='alerta alerta-exito'>
        <?=$_SESSION['completado']?>
    </div>
    <?php elseif(isset($_SESSION['errores']['entrada'])): ?>
    <div class='alerta alerta-error'>
        <?=$_SESSION['errores']['entrada']?>
    </div>
    <?php endif;?>
    <!--FIN DE MOSTRAR ERRORES DE GUARDAR USUARIO-->

    <h1>Crear Entradas</h1>

    <p>
        Añade nuevas entradas al blog para que los usuarios puedan verlas.
    </p>

    <form action="guardar-entradas.php" method="POST">
        <br>


        <label for="titulo"> Titulo</label>
        <input type="text" name="titulo" id="titulo">
        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'titulo') : ''; ?>


        <label for="descripcion"> Descripcion</label>
        <textarea name="descripcion" id="descripcion" cols="80" rows="5"></textarea>
        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'descripcion') : ''; ?>


        <label for="categoria"> Categoria</label>
        <select name="categoria" id="categoria">

            <?php $categorias = conseguirCategorias($db);
                        
              if(!empty($categorias)):
              //<!--recorro las categorias e imprimo en pantalla en lista-->
                while($categoria = mysqli_fetch_assoc($categorias)): 
            ?>
            <option value="<?=$categoria['id']?>"><?=$categoria['nombre']?></option>
            <?php 
                 endwhile;
               endif;        
                    ?>

        </select>
        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'categoria') : ''; ?>

        <input type="submit" value="Guardar">

    </form>
    <?php borrarErrores();?>

</div>


<?php include_once './includes/footer.php'?>