<?php require_once './includes/cabecera.php' ?>
<!--BARRA LATERAL-->
<?php require_once './includes/lateral.php'?>
<div id="principal">
    <h1>Todas las Entradas</h1>



    <!--obtengo las entradas de helpers-->
    <?php $entradas = conseguirEntradas($db);


            if(!empty($entradas)):
            //<!--recorro las entradas e imprimo en pantalla en lista-->
            while($entrada = mysqli_fetch_assoc($entradas)): 
            //var_dump($entrada);

     ?>

    <article class="entrada">
        <a href="entrada.php?id=<?=$entrada['id']?>">
            <h2><?=$entrada['titulo']?></h2>

            <span class='fecha'><?= $entrada['categoria']. ' | '. $entrada['fecha'] ?></span>
            <p>
                <?=substr($entrada['descripcion'], 0, 180). "..."; ?>
            </p>
        </a>
    </article>

    <?php 
    endwhile;
    endif;
    ?>
    <!--fin de las entradas-->



</div>


<?php include_once './includes/footer.php'?>