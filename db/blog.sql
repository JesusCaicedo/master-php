-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 30-01-2021 a las 08:04:17
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `blog`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`) VALUES
(1, 'Accion'),
(2, 'deportes'),
(13, 'Juegos'),
(14, 'comedia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entradas`
--

DROP TABLE IF EXISTS `entradas`;
CREATE TABLE IF NOT EXISTS `entradas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(255) NOT NULL,
  `categoria_id` int(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` mediumtext DEFAULT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entrada_usuario` (`usuario_id`),
  KEY `fk_entrada_categoria` (`categoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entradas`
--

INSERT INTO `entradas` (`id`, `usuario_id`, `categoria_id`, `titulo`, `descripcion`, `fecha`) VALUES
(1, 11, 1, 'primera entrada', 'es simplemente texto de relleno de la industria de la impresión y la composición tipográfica. Lorem Ipsum ha sido el texto de relleno estándar de la industria desde el siglo XVI, cuando un impresor desconocido tomó una galera de tipos y la mezcló para hacer un libro de muestras tipo. Ha sobrevivido no solo a cinco siglos, sino también al salto a la composición tipográfica electrónica, permaneciendo esencialmente sin cambios. Se popularizó en la década de 1960 con el lanzamiento de hojas de Letraset que contienen pasajes de Lorem Ipsum y, más recientemente, con software de autoedición como Aldus PageMaker que incluye versiones de Lorem Ipsum.', '2021-01-29'),
(3, 14, 1, 'Segunda entrada', 'es simplemente texto de relleno de la industria de la impresión y la composición tipográfica. Lorem Ipsum ha sido el texto de relleno estándar de la industria desde el siglo XVI, cuando un impresor desconocido tomó una galera de tipos y la mezcló para hacer un libro de muestras tipo. Ha sobrevivido no solo a cinco siglos, sino también al salto a la composición tipográfica electrónica, permaneciendo esencialmente sin cambios. Se popularizó en la década de 1960 con el lanzamiento de hojas de Letraset que contienen pasajes de Lorem Ipsum y, más recientemente, con software de autoedición como Aldus PageMaker que incluye versiones de Lorem Ipsum.', '2021-01-29'),
(4, 11, 2, 'tercera entrada', 'es simplemente texto de relleno de la industria de la impresión y la composición tipográfica. Lorem Ipsum ha sido el texto de relleno estándar de la industria desde el siglo XVI, cuando un impresor desconocido tomó una galera de tipos y la mezcló para hacer un libro de muestras tipo. Ha sobrevivido no solo a cinco siglos, sino también al salto a la composición tipográfica electrónica, permaneciendo esencialmente sin cambios. Se popularizó en la década de 1960 con el lanzamiento de hojas de Letraset que contienen pasajes de Lorem Ipsum y, más recientemente, con software de autoedición como Aldus PageMaker que incluye versiones de Lorem Ipsum.', '2021-01-29'),
(5, 10, 1, 'prue', 'prue', '2021-01-30'),
(6, 10, 1, 'prue', 'prue', '2021-01-30'),
(7, 10, 13, 'gta', 'juego de guerra', '2021-01-30'),
(8, 10, 1, 'gta1a', 'adsd', '2021-01-30'),
(9, 10, 1, 'uyuy766', 'uyyugu', '2021-01-30'),
(10, 10, 1, '57yutu', 'utytghh', '2021-01-30'),
(11, 10, 1, 'gta', 'ww', '2021-01-30'),
(12, 10, 2, 'esto es una prueba', 'esto es una prueba', '2021-01-30'),
(13, 10, 14, 'comedia', 'comedia', '2021-01-30'),
(14, 10, 1, 'jsus', 'jeus', '2021-01-30'),
(15, 10, 1, 'wew', 'wewe', '2021-01-30'),
(16, 10, 1, 'prueb', 'jesus', '2021-01-30'),
(17, 10, 1, 'gta', 'hh', '2021-01-30'),
(18, 10, 13, 'wewewewe', 'hgjhg', '2021-01-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `email`, `password`, `fecha`) VALUES
(1, 'jesus', 'caicedo', 'jesus1996eduardo@gmail.com', '$2y$04$9YbttozGDpB8tchRq6WUxu9mL7PabQCer1GPcE2GjNaAzkFhj3Vhy', '2021-01-26'),
(6, 'eduard', 'caicedo', 'je1996sus_@hotmail.com', '$2y$04$Rhe7C9Zrq1gXd0vrOlCcduaAloxJMNCAG0a3HVH1au7ZC9vWs0lpO', '2021-01-26'),
(7, 'jhonatan', 'caicedo', 'jhonatan@hotmail.com', '$2y$04$swAmSqX6TYysxbF.oYk34OAgE0OWI6G2umLoW.cRrHHQPX42ub72.', '2021-01-26'),
(8, 'camilo', 'perez', 'camiloperez@hotmail.com', '$2y$04$z9lyfLn/vClev0fxubaNTeW3tmByqCE9TV8J.dFDXeYGZrMZBCD3G', '2021-01-26'),
(9, 'jesus eduardo', 'caicedo ruano', 'jesus.jesus@hotmail.com', '$2y$04$Hgw4arW.Vg1LQVGWQdxMgOl6MXQunjR5yupiZjPDoSIBjkfy2RkMG', '2021-01-27'),
(10, 'paola', 'paola', 'paola@paola.com', '$2y$04$Ajoc5E2yJnoUbgjB7SnvyeNrG3imrRhKAcSRehIkU9MtHhfk4z3/.', '2021-01-28'),
(11, 'jesus', 'caicedo', 'jesus@jesus.com', '$2y$04$nt8QrLJMH7bxvGzHuE7vNusn/IRSXRmK3FhT810JCI91feJqjAPv.', '2021-01-29'),
(14, 'david', 'caicedo', 'david@david.com', '$2y$04$2/qIUXXuk3R/JJ4R3.4yieZTJuN7AIZgP.5S4YmRrvMCKlUFqQMPW', '2021-01-29');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `entradas`
--
ALTER TABLE `entradas`
  ADD CONSTRAINT `fk_entrada_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `fk_entrada_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
