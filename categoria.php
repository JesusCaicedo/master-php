  <?php require_once './includes/helpers.php' ?>
  <?php require_once './includes/conexion.php' ?>

  <?php
        $categoria_actual = conseguirCategoria($db,$_GET['id']);
        if(!isset($categoria_actual['id'])){
            header("location: index.php");
        }
    ?>


  <?php require_once './includes/cabecera.php' ?>
  <!--BARRA LATERAL-->
  <?php require_once './includes/lateral.php'?>
  <div id="principal">


      <h1>Entradas de <?= $categoria_actual['nombre']?></h1>

      <!--obtengo las entradas de helpers-->
      <?php $entradas = conseguirEntradas($db,null, $_GET['id']);


            if(!empty($entradas) && mysqli_num_rows($entradas) >= 1):
            //<!--recorro las entradas e imprimo en pantalla en lista-->
            while($entrada = mysqli_fetch_assoc($entradas)): 
            //var_dump($entrada);

     ?>

      <article class="entrada">
          <a href="entrada.php?id=<?=$entrada['id']?>">
              <h2><?=$entrada['titulo']?></h2>

              <span class='fecha'><?= $entrada['categoria']. ' | '. $entrada['fecha'] ?></span>
              <p>
                  <?=substr($entrada['descripcion'], 0, 180). "..."; ?>
              </p>
          </a>
      </article>

      <?php 
    endwhile;
    else:
    ?>

      <div class="alerta"> No hay entradas en esta categoria</div>
      <?php endif; ?>
      <!--fin de las entradas-->



  </div>


  <?php include_once './includes/footer.php'?>