<?php

//var_dump($_POST);

if(isset($_POST['submit'])){

require_once './includes/conexion.php';

//validamos si la sesion no existe, de lo contrario la creamos
if(!isset($_SESSION)){
session_start();
}


//recogemos los datos y asignamos false si estan vacios, con mysql_escape evitamos las inyecciones SQL
    $nombre = isset($_POST['nombre']) ? mysqli_escape_string($db, $_POST['nombre']) : false;
    $apellido = isset($_POST['apellido']) ? mysqli_escape_string($db, $_POST['apellido']) : false;
    $email = isset($_POST['email']) ? mysqli_escape_string($db, trim($_POST['email'])) : false;
    $usuario = $_SESSION['usuario']['id'];


    //array de errores
    $errores = array();

//Validamos si los datos no estan vacios y ademas si no es numerico y validamos que no contenga numeros
//validamos el nombre    
    if(!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/",$nombre)){
        $nombre_validado=true;
    }else{
        $nombre_validado=false;
        $errores['nombre'] = "El nombre no es valido";
    }

    //validamos el apellido
    if(!empty($apellido) && !is_numeric($apellido) && !preg_match("/[0-9]/",$apellido)){
            $apellido_validado=true;
        }else{
            $apellido_validado=false;
            $errores['apellido'] = "El apellido no es valido";
        }
        //validamos el email
        if(!empty($email) && filter_var($email,FILTER_VALIDATE_EMAIL)){
            $email_validado=true;
        }else{
            $email_validado=false;
            $errores['email'] = "El email no es valido";
        }

     
        //seteamos una variable en false para luego cambiar los estados y saber que realiza, guardar o no el usuario
        $actualiza_usuario = false;
        if(count($errores) == 0){
            $actualiza_usuario = true;


            //Validamos si el correo ya existe

            $sql = "SELECT id, email FROM usuarios WHERE email='$email'";
            $isset_email = mysqli_query($db,$sql);
            $isset_user = mysqli_fetch_assoc($isset_email);

            //validamos errores del sql
            //var_dump(mysqli_error($db));
            // die();

            if($isset_user['id'] == $usuario['id'] || empty($isset_user)){

                
            //actualizar el usuario en su tabla correspondiente
           
            //$sql = "INSERT INTO usuarios VALUES (null, '$nombre','$apellido','$email','$password_segura',CURDATE())";
            $sql = "UPDATE usuarios SET nombre='$nombre', apellidos='$apellido',email='$email' WHERE id='$usuario'";

            $actualizar = mysqli_query($db,$sql);
            
            //validamos errores del sql
            //var_dump(mysqli_error($db));
            //die();
            
            if($actualizar){

                $_SESSION['usuario']['nombre']= $nombre;
                $_SESSION['usuario']['apellidos']=$apellido;
                $_SESSION['usuario']['email']=$email;

                $_SESSION['completado'] = "El usuario se actualizo exitosamente";
            }else{
                $_SESSION['errores']['general'] = "Fallo al actualizar  el usuario";
            }

            }else{
               $_SESSION['errores']['general'] = "El correo ya existe";

            }

        }else{

            /*si hay errores, los guardamos en una session indicamos 
            reidereccionar al index, para requeriel la funcion de mostrarError del archivo helper*/
            $_SESSION['errores'] = $errores;
        }
}

 header('location: misdatos.php');

 ?>