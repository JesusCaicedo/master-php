<?php require_once './includes/redireccion.php'?>
<?php require_once './includes/cabecera.php' ?>
<?php require_once './includes/lateral.php'?>


<div id="principal">

    <!--MOSTRAMOS RESULTADOS DE GUARDAR USUARIO-->
    <?php if(isset($_SESSION['completado'])): ?>
    <div class='alerta alerta-exito'>
        <?=$_SESSION['completado']?>
    </div>
    <?php elseif(isset($_SESSION['errores']['general'])): ?>
    <div class='alerta alerta-error'>
        <?=$_SESSION['errores']['general']?>
    </div>
    <?php endif;?>
    <!--FIN DE MOSTRAR ERRORES DE GUARDAR USUARIO-->

    <h1>Crear Categorias</h1>

    <p>
        Añade nuevas categorias al blog para que los usuarios puedan utilizarlas al crear sus entradas.
    </p>

    <form action="guardar-categoria.php" method="POST">
        <br>
        <label for="categoria"> Nombre de la Categoria</label>
        <input type="text" name="nombre" id="nombre">

        <input type="submit" value="Guardar">

    </form>
    <?php borrarErrores();?>

</div>


<?php include_once './includes/footer.php'?>