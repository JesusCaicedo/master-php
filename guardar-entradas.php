<?php

if(isset($_POST)){

    require_once './includes/conexion.php';

    $titulo = isset($_POST['titulo']) ? mysqli_escape_string($db, $_POST['titulo']) : false;
    $descripcion = isset($_POST['descripcion']) ? mysqli_escape_string($db, $_POST['descripcion']) : false;
    $categoria = isset($_POST['categoria']) ?  (int)$_POST['categoria'] : false;
    $usuario = $_SESSION['usuario']['id'];


   $errores = array();
  
    
    if(!empty($titulo) && !is_numeric($titulo) && !preg_match("/[0-9]/",$titulo)){ 
        $titulo_validado=true;
    }else{
        $titulo_validado=false;
        $errores['titulo'] = "El titulo de la entrada no es valido";
    }
    if(!empty($descripcion)){
        $descripcion_validado=true;
    }else{
        $descripcion_validado=false;
        $errores['descripcion'] = "La descripcion no es valida";
    }
    if(!empty($categoria) && is_numeric($categoria)){
        $categoria_validado=true;
    }else{
        $categoria_validado=false;
        $errores['categoria'] = "La categoria no es valida";
    }


        if(count($errores) == 0){
            if(isset($_GET['editar'])){
                
                $entrada_id = $_GET['editar'];
                $usuario_id = $_SESSION['usuario']['id'];

            $sql = "UPDATE  entradas SET titulo='$titulo',descripcion='$descripcion',categoria_id=$categoria ".
            "WHERE id = $entrada_id AND usuario_id = $usuario_id";

            }else{
            $sql = "INSERT INTO entradas VALUES (null,'$usuario', '$categoria','$titulo','$descripcion',CURDATE())";

            }

            $guardar = mysqli_query($db,$sql);

            if($guardar){
                $_SESSION['completado'] = "La entrada se registro exitosamente";
            }else{
                $_SESSION['errores']['entrada'] = "Fallo al guardar la entrada";
            }

        }else{
               $_SESSION['errores']['entrada'] = "Fallo al guardar la entrada";
               $_SESSION['errores'] = $errores;


        }

}

if (count($errores)  == 0 ){
 header('location: index.php');

}else{
    if(isset($_GET['editar'])){ 
         header('location: editarEntrada.php?id='.$_GET['editar']);
    }else{
        header('location: crearEntradas.php');

    }

}


?>